#
# Install and confifgure a Phabricator node.
#
class nextdoor::profile::phabricator(

) inherits ::nextdoor::profile::params {

  $serveraliases = ['localhost', 'phabricator', 'phab']

  class { '::apache': default_vhost => false, }

  apache::vhost { 'phab-tcp80-redirect':
    default_vhost => true,
    port          => 80,
    servername    => $::fqdn,
    serveraliases => $serveraliases,
    docroot       => $::apache::docroot,
    rewrites      => [
      {
        comment      => 'redirect tcp/80 to tcp/443',
        rewrite_cond => ['%{HTTPS} !=on'],
        rewrite_rule => ['^/?(.*) https://%{SERVER_NAME}/$1 [R,L]'],
      }
    ],
  }

  apache::vhost { 'phab-tcp443':
    port          => 443,
    servername    => $::fqdn,
    serveraliases => $serveraliases,
    docroot       => "${::apache::docroot}/phab",
  }

  $mysql_service_provider = $::virtual ? {
    'docker' => 'base',
    default  => undef
  }

  class { '::mysql::server':
    service_provider => $mysql_service_provider,
  } ->

  class { '::phabricator': }
}
