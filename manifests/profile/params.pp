#
# Utility class for all profiles in the ::nextdoor::profiles namespace.
#
class nextdoor::profile::params {

  unless 'Ubuntu' == $::operatingsystem {
    fail("OS \'${::operatingsystem}\' not supported by this module!")
  }

}
