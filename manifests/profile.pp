#
# Stub baseclass for the ::nextdoor::profile namespace.
#
class nextdoor::profile(
) inherits ::nextdoor::profile::params {

  require ::nextdoor
}
