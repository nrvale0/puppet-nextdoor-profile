#!/usr/bin/env bats

@test "Mysqld is installed" {
  run dpkg -l mysql-server
  [ "$status" -eq 0 ]
}

@test "Apache is installed" {
  run dpkg -l apache2
  [ "$status" -eq 0 ]
}