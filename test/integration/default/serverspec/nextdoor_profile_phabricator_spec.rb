require 'net/http'
require 'uri'

require 'serverspec'

set :backend, :exec

describe "phabricator via defaults" do
  
  it "is listening on port tcp/80" do
    expect(port(80)).to be_listening
  end

  it "is listenting on tcp/443" do
    expect(port(443)).to be_listening
  end

  def http_request(uri, proto=nil)
    uri = URI(uri)
    req = Net::HTTP::Get.new(uri.request_uri)
    req['X-Forwarded-Proto'] = proto if proto
    Net::HTTP.start(uri.hostname, uri.port) {|http| http.request(req)}
  end
  
  it "should redirect tcp/80 to tcp/443" do
    res = http_request('http://localhost')
    expect(res.code).to eq('302')
  end

  it "has a running Apache daemon" do
    expect(service('apache2')).to be_running
  end
  
  it "has a running MySQL daemon" do
    expect(service('mysqld')).to be_running            
  end
           
end
